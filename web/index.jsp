<%-- 
    Document   : index.jsp
    Created on : 18/12/2020, 03:21:45 PM
    Author     : 1151624-1151628
--%>

<%@page import="DAO.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="css/bootstrap.css" />

        <title>Rutas transporte publico</title>
    </head>

    <body   background="imagenes/transporte.j" style="background-repeat: no-repeat; vertical-align: unset;">
        <header>
            <div class="container">
                <h2>Rutas Transporte Publico</h2>
            </div>
        </header>

        <div class="container" >
            <br>
            <h2 class="text-center">¿A donde quieres ir?</h2>
            <br>
            <br>
            <br>

            <form action="prueba.do">
                <div class="form-group container input-group" style="width: 40%">
                    <label>Mi ubicacion  </label><br>


                    <%
                        Conexion c = new Conexion();
                        c.databaseconect("bdrutas");
                        String sql = "select * from barrios order by nombre_barrio";
                        c.setRs(sql);
                    %> 

                    <select id="seleccion1" name="seleccion1" class="form-control" onchange="ShowSelected();" required>
                        <option>Seleccionar barrio</option>
                        <% while (c.getRs().next()) {

                        %>

                        <option value="<%= c.getRs().getInt("id")%>"><%= c.getRs().getString("nombre_barrio")%></option>

                        <%}
                        %>

                    </select>

                    <input type="hidden" value="" name="codigouno" id="codigouno" placeholder="">
                    <input type="hidden" value="" name="codigodos" id="codigodos" placeholder="">


                    <script type="text/javascript">
                        function ShowSelected()
                        {
                            /* Para obtener el valor */
                            var cod1 = document.getElementById("seleccion1").value;
                            var cod2 = document.getElementById("seleccion2").value;
                            //alert("-" + cod1 + "-" + cod2 + "-");

                            document.getElementById("codigouno").value = cod1;
                            document.getElementById("codigodos").value = cod2;
                        }
                    </script>

                </div>
                <br>
                <div class="form-group container input-group" style="width: 40%">
                    <label>Destino  </label><br>

                    <%
                        Conexion c2 = new Conexion();
                        c2.databaseconect("bdrutas");
                        String sql2 = "select * from barrios order by nombre_barrio";
                        c2.setRs(sql2);
                    %>

                    <select required id="seleccion2" name="seleccion2" class="form-control" onchange="ShowSelected();" >
                        <option>Seleccionar barrio</option>
                        <% while (c2.getRs().next()) {

                        %>

                        <option value="<%= c2.getRs().getInt("id")%>"><%= c2.getRs().getString("nombre_barrio")%></option>
                        <%}
                        %>

                    </select>
                </div>

                <br>
                <div class="form-group">
                    <div class="col-xs-12" style="text-align: center">
                        <p class="btn col-lg-5"></p>
                        <p class="btn col-lg-5"></p>
                    </div>

                </div>
                <div class="form-group"><br><br></div>


                <div class="form-group">
                    <div class="col-xs-12" style="text-align: center">
                        <p class="btn col-lg-5"></p>
                        <button class="btn col-lg-2" id="botonLogin" >Buscar Rutas</button>
                        <p class="btn col-lg-5"></p>
                    </div>

                </div>

            </form>
        </div>

        <footer>
            <div>
                <p>Universidad Francisco de Paula Santander - Ingenieria de Sistemas 1151624 - 1151628</p>
                <p>©2020</p>

            </div>
        </footer>

        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/validaciones.js"></script>
    </body>

</html>

