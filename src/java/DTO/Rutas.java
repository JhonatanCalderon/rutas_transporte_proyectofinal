/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "rutas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rutas.findAll", query = "SELECT r FROM Rutas r")
    , @NamedQuery(name = "Rutas.findById", query = "SELECT r FROM Rutas r WHERE r.id = :id")
    , @NamedQuery(name = "Rutas.findByB1", query = "SELECT r FROM Rutas r WHERE r.b1 = :b1")
    , @NamedQuery(name = "Rutas.findByB2", query = "SELECT r FROM Rutas r WHERE r.b2 = :b2")
    , @NamedQuery(name = "Rutas.findByB3", query = "SELECT r FROM Rutas r WHERE r.b3 = :b3")})
public class Rutas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "b1")
    private int b1;
    @Basic(optional = false)
    @Column(name = "b2")
    private int b2;
    @Basic(optional = false)
    @Column(name = "b3")
    private int b3;

    public Rutas() {
    }

    public Rutas(Integer id) {
        this.id = id;
    }

    public Rutas(Integer id, int b1, int b2, int b3) {
        this.id = id;
        this.b1 = b1;
        this.b2 = b2;
        this.b3 = b3;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getB1() {
        return b1;
    }

    public void setB1(int b1) {
        this.b1 = b1;
    }

    public int getB2() {
        return b2;
    }

    public void setB2(int b2) {
        this.b2 = b2;
    }

    public int getB3() {
        return b3;
    }

    public void setB3(int b3) {
        this.b3 = b3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rutas)) {
            return false;
        }
        Rutas other = (Rutas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.Rutas[ id=" + id + " ]";
    }
    
}
