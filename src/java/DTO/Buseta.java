/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "buseta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Buseta.findAll", query = "SELECT b FROM Buseta b")
    , @NamedQuery(name = "Buseta.findById", query = "SELECT b FROM Buseta b WHERE b.id = :id")
    , @NamedQuery(name = "Buseta.findByEmpresa", query = "SELECT b FROM Buseta b WHERE b.empresa = :empresa")
    , @NamedQuery(name = "Buseta.findByTiempo", query = "SELECT b FROM Buseta b WHERE b.tiempo = :tiempo")})
public class Buseta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "empresa")
    private String empresa;
    @Column(name = "tiempo")
    private Integer tiempo;
    @Lob
    @Column(name = "urlmapa")
    private String urlmapa;

    public Buseta() {
    }

    public Buseta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getTiempo() {
        return tiempo;
    }

    public void setTiempo(Integer tiempo) {
        this.tiempo = tiempo;
    }

    public String getUrlmapa() {
        return urlmapa;
    }

    public void setUrlmapa(String urlmapa) {
        this.urlmapa = urlmapa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Buseta)) {
            return false;
        }
        Buseta other = (Buseta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.Buseta[ id=" + id + " ]";
    }
    
}
