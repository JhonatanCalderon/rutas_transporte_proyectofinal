/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ASUS
 */
public class prueba extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            //out.println("funciona alv");
            /* TODO output your page here. You may use following sample code. */

            String barrio1select = request.getParameter("codigouno");
            String barrio2select = request.getParameter("codigodos");

            //Integer opcion1 = Integer.parseInt(request.getParameter("solo1"));
            //Integer opcion2 = Integer.parseInt(request.getParameter("solo2"));

            /*if (opcion1 == 0 || opcion2 == 0) {
                String mens = "_______________________________________";
                request.setAttribute("mensaje", mens);
                request.getRequestDispatcher("index.jsp").forward(request, response);
            } else if (barrio1select.equals(barrio2select)) {
                String negacion = "No se puede seleccionar el mismo barrio de origen y destino";
                request.setAttribute("mensaje", negacion);
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
             */
            Conexion c3 = new Conexion();
            c3.databaseconect("bdrutas");

            //boolean validar = false;
            //String sql3 = "select * from rutas where (barriouno like '78' or barriodos like '78' or barriotres like '78') and (barriouno like '42' or barriodos like '42' or barriotres like '42') ";
            String sql3 = "select * from rutas where (barriouno like '" + barrio1select + "' or barriodos like '" + barrio1select + "' or barriotres like '" + barrio1select + "' or barriocuatro like '" + barrio1select + "' or barriocinco like '" + barrio1select + "' or barrioseis like '" + barrio1select + "' or barriosiete like '" + barrio1select + "') and (barriouno like '" + barrio2select + "' or barriodos like '" + barrio2select + "' or barriotres like '" + barrio2select + "'or barriocuatro like '" + barrio2select + "' or barriocinco like '" + barrio2select + "' or barrioseis like '" + barrio2select + "' or barriosiete like '" + barrio2select + "') ";

            c3.setRs(sql3);
            if (!c3.getRs().isBeforeFirst()) {
                //String error = "No data";
                //request.setAttribute("mensajes", error);
                request.getRequestDispatcher("html/informacion.html").forward(request, response);
            } else {
                request.setAttribute("saludo", sql3);
                request.getRequestDispatcher("html/pruebajsp.jsp").forward(request, response);
            }
        } catch (Exception e) {
            out.println("no se ha encontrado nada");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
