Proyecto Rutas de Transporte Publico
Este proyecto se realizó como Examen Final de la materia de Computación en la nube
correspondiente al Pensum 115 del programa Ingenieria de Sistemas de la Universidad Francisco de Paula Santander
Aplicación web para conocer las diferentes rutas de las busetas 
de la ciudad de Cúcuta.

Autores
Angela Mildred Acevedo Rodriguez - 1151628 --> enlace a gitlab: https://gitlab.com/1151628

Jhonatan Raúl Calderón Acevedo - 1151624 --> enlace a gitlab: https://gitlab.com/1151624

Estudiantes 8vo Semestre Ingenieria de Sistemas 

Desarrollo

- Herramientas

Nube de Google : Máquina virtual, dirección ip estática, reglas de cortafuegos, servidor tomcat para despliegue de la app

Nube de Amazon: Máquina virtual ec2, dirección ip estática, servicio de mariadb
