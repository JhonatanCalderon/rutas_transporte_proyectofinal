-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-12-2020 a las 18:13:36
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdrutas`
--
CREATE DATABASE IF NOT EXISTS `bdrutas` DEFAULT CHARACTER SET utf8mb4;
USE `bdrutas`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barrios`
--

CREATE TABLE `barrios` (
  `id` int(3) NOT NULL,
  `nombre_barrio` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `barrios`
--

INSERT INTO `barrios` (`id`, `nombre_barrio`) VALUES
(1, 'San Luis'),
(2, 'Chapinero'),
(3, 'San Martín'),
(4, 'El Callejón'),
(5, 'Centro'),
(6, 'El Contento'),
(7, 'La Playa'),
(8, 'La Libertad'),
(9, 'Latino'),
(10, 'Aeropuerto'),
(11, 'Atalaya'),
(12, 'Aniversario l'),
(13, 'Antonia Santos'),
(14, 'Belén'),
(15, 'Belisario'),
(16, 'El Páramo'),
(17, 'La sexta'),
(18, 'Acuarela'),
(19, 'Barrio Blanco'),
(20, 'Bellavista'),
(21, 'Brisas del pamplonita'),
(22, 'Caobos'),
(23, 'Ceiba l'),
(24, 'Ceiba ll'),
(25, 'Colsag'),
(26, 'Condado de Castilla'),
(27, 'El Lago'),
(28, 'El Rosal'),
(29, 'Govica'),
(30, 'Hacaritama'),
(31, 'Hurapanes'),
(32, 'La capillana'),
(33, 'La castellana'),
(34, 'La Ceiba'),
(35, 'La Primavera'),
(36, 'La Rinconada'),
(37, 'La Riviera'),
(38, 'Las Almeidas'),
(39, 'Libertadores'),
(40, 'Los Acacios'),
(41, 'Los Pinos'),
(42, 'Los Proceres'),
(43, 'Manolo Lemus'),
(44, 'Mirador campestre'),
(45, 'Palma Real'),
(46, 'Parque central'),
(47, 'Parque de las brisas'),
(48, 'Parque Real'),
(49, 'Parques recidenciales'),
(50, 'Popular'),
(51, 'Portal de prados'),
(52, 'Prados'),
(53, 'Prados ll'),
(54, 'Prados Club'),
(55, 'Quinta Bosh'),
(56, 'Quinta Oriental'),
(57, 'Quinta Velez'),
(58, 'Rincón de prados'),
(59, 'Santa Clara'),
(60, 'Santa Lucía'),
(61, 'San Isidro'),
(62, 'San Remo'),
(63, 'Torre Real'),
(64, 'Urbanización Galicia'),
(65, 'Urbanización la esperanza'),
(66, 'Urbanización Torre Molinos'),
(67, 'Valparaiso suit'),
(68, 'Villas del prado'),
(69, 'Villa Real'),
(70, 'Aguas calientes'),
(71, 'Bellavista ll'),
(72, 'Boconó'),
(73, 'Bogotá'),
(74, 'El Triunfo'),
(75, 'Luis Pérez Hernández'),
(76, 'La Unión'),
(77, 'Morelly'),
(78, 'Mujeres con futuro'),
(79, 'Nuevo milenio'),
(80, 'Policarpa'),
(81, 'San Mateo'),
(82, 'Santa Ana'),
(83, 'San Juanito'),
(84, 'Urbanización Betel'),
(85, 'Urbanización el Cují'),
(86, 'Urbanización la Carolina'),
(87, 'Urbanización las Margaritas'),
(88, 'Urbanización Sana Ana'),
(89, 'Valle Esther'),
(90, '13 de Marzo'),
(91, 'Alto pamplonita'),
(92, 'Aniversario ll'),
(93, 'Condominio Prado del este'),
(94, 'Conjunto cerrado estación del este'),
(95, 'El Higuerón'),
(96, 'Heliopolis'),
(97, 'Isla de Fantasía'),
(98, 'La Quinta'),
(99, 'Prados del este'),
(100, 'San José'),
(101, 'Santa Teresita'),
(102, 'Siglo XXl'),
(103, 'Torcoroma'),
(104, 'Torcoroma ll'),
(105, 'Urbanización Cañafístolo'),
(106, 'Urbanización la Alameda'),
(107, 'Urbanización la Florida'),
(108, 'Urbanización Nueva esperanza'),
(109, 'Urbanización Nuevo Escobal'),
(110, 'Urbanización Santa Clara'),
(111, 'Urbanización San Diego'),
(112, 'Urbanización San Matín'),
(113, 'Urbanización Santillana'),
(114, 'Urbanización Terranova'),
(115, 'Valle Bonito'),
(116, 'Villa Camila'),
(117, 'Viejo Escobal'),
(118, 'Villas de San Diego'),
(119, 'Alcalá'),
(120, 'Ciudad Jardín'),
(121, 'El Bosque'),
(122, 'Gratamira'),
(123, 'Guaimaral'),
(124, 'La Merced'),
(125, 'Lleras Restrepo'),
(126, 'Paraíso'),
(127, 'Pescadero-Colpet'),
(128, 'Portachuelo'),
(129, 'Prados del Norte'),
(130, 'San Eduardo'),
(131, 'Santa Helena'),
(132, 'Sevilla'),
(133, 'Tasajero'),
(134, 'Urbanización Gualanday'),
(135, 'Urbanización la Mar'),
(136, 'Urbanización Libertadores Royal'),
(137, 'Urbanización Niza'),
(138, 'Zona industrial'),
(139, 'Zona Franca'),
(140, 'Zulima'),
(141, 'Brisas del Aeropuerto'),
(142, 'Brisas de los Molinos'),
(143, 'Brisas del Norte'),
(144, 'Brisas del Paraíso'),
(145, 'Brisas del Porvenir'),
(146, 'Caño Limón'),
(147, 'Carlos García Lozada'),
(148, 'Carlos Pizarro'),
(149, 'Cecilia Castro'),
(150, 'Cerro la Cruz'),
(151, 'Cerro Norte'),
(152, 'Colinas del Salado'),
(153, 'Colinas de la Victoria'),
(154, 'Cumbres del Norte'),
(155, 'Divino Niño'),
(156, 'El Cerrito'),
(157, 'El Dorado'),
(158, 'El Porvenir'),
(159, 'El Porvenir Bajo'),
(160, 'El Salado'),
(161, 'Florida Blanca'),
(162, 'García Herreros'),
(163, 'La Concordia'),
(164, 'La Isla'),
(165, 'La Insula'),
(166, 'Limonar del Norte'),
(167, 'Los Laureles'),
(168, 'María Auxiliadora'),
(169, 'Maria Paz'),
(170, 'Molinos del Norte'),
(171, 'Nueva Sexta'),
(172, 'Panamericano'),
(173, 'Portal de las Américas'),
(174, 'Rafael Núñez'),
(175, 'San Jerónimo'),
(176, 'Simón Bolivar'),
(177, 'Toledo Plata'),
(178, 'Trigal del Norte'),
(179, 'Urbanización las Américas'),
(180, 'Urbanización los Girasoles'),
(181, 'Urbanización Luis David Flórez'),
(182, 'Villas de las Américas'),
(183, 'Villas de los Tejares'),
(184, 'Villa Juliana'),
(185, 'Virgilio Barco'),
(186, 'Buenos Aires'),
(187, 'Camilo Daza'),
(188, 'Claret'),
(189, 'Comuneros'),
(190, 'Crispin Durán'),
(191, 'Juan Bautista Scalabrini'),
(192, 'La Florida'),
(193, 'La Hermita'),
(194, 'La Laguna'),
(195, 'Las Américas'),
(196, 'Motilones'),
(197, 'Nueva Colombia'),
(198, 'Ospina Perez'),
(199, 'Rosal del Norte'),
(200, 'Tucunaré'),
(201, '7 De Agosto'),
(202, '13 De Mayo'),
(203, 'Atalaya l Etapa'),
(204, 'Atalaya ll Etapa'),
(205, 'Belisario Betancour'),
(206, 'Carlos Ramírez París'),
(207, 'Cerro Pico'),
(208, 'Cúcuta 75'),
(209, 'Doña Nidia'),
(210, 'El Desierto'),
(211, 'El Oasis'),
(212, 'El Progreso'),
(213, 'El Rodeo'),
(214, 'Juan Pablo ll'),
(215, 'Juana Rangel'),
(216, 'La Coralina'),
(217, 'La Victoria'),
(218, 'Los Almendros'),
(219, 'Minuto de Dios'),
(220, 'Nuevo Horizonte'),
(221, 'Niña Ceci'),
(222, 'Palmeras'),
(223, 'Sabana Verde'),
(224, '28 de Febrero'),
(225, 'Barrio Nuevo'),
(226, 'Belén de Umbría'),
(227, 'Cundinamarca'),
(228, 'Carora'),
(229, 'Divina Pastora'),
(230, 'Loma de Bolivar'),
(231, 'Los Alpes'),
(232, 'Rudesindo Soto'),
(233, 'San Miguel'),
(234, 'Urbanización Brisa de la Hermita'),
(235, 'Valles del Rodeo'),
(236, 'Villa de la Paz'),
(237, 'Alfonso López'),
(238, 'Camilo Torres'),
(239, 'Circunvalación'),
(240, 'Cuberos Niño'),
(241, 'Gaitán'),
(242, 'Galán '),
(243, 'La Cabrera'),
(244, 'Magdalena'),
(245, 'Puente Barco'),
(246, 'Santander'),
(247, 'Santo Domíngo'),
(248, 'San Rafael');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buseta`
--

CREATE TABLE `buseta` (
  `id` int(3) NOT NULL,
  `empresa` varchar(35) DEFAULT NULL,
  `tiempo` int(4) DEFAULT NULL,
  `urlmapa` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `buseta`
--

INSERT INTO `buseta` (`id`, `empresa`, `tiempo`, `urlmapa`) VALUES
(1, 'Trasan', 45, 'https://www.google.com/maps/d/embed?mid=1KABCN7GnhmBWubLhr8JxB6Sojn_Hga0N'),
(3, 'Guasimales', 30, 'https://www.google.com/maps/d/embed?mid=14Wyzo1BrLDiqU_zmbmFLef91thTrvqrg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas`
--

CREATE TABLE `rutas` (
  `id` int(3) NOT NULL,
  `barriouno` int(3) NOT NULL,
  `barriodos` int(11) NOT NULL,
  `barriotres` int(11) NOT NULL,
  `barriocuatro` int(3) NOT NULL,
  `barriocinco` int(3) NOT NULL,
  `barrioseis` int(3) NOT NULL,
  `barriosiete` int(3) NOT NULL,
  `empresa` varchar(35) NOT NULL,
  `tiempo` int(8) NOT NULL,
  `urlmapa` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rutas`
--

INSERT INTO `rutas` (`id`, `barriouno`, `barriodos`, `barriotres`, `barriocuatro`, `barriocinco`, `barrioseis`, `barriosiete`, `empresa`, `tiempo`, `urlmapa`) VALUES
(1, 248, 228, 230, 5, 8, 0, 0, 'Trasan', 12, 'https://www.google.com/maps/d/u/0/embed?mid=1IUzcZX1N9yMg9M1ju8gCb1O2ywyAb8DL'),
(2, 212, 207, 248, 2, 189, 188, 0, 'Trasan', 4, 'https://www.google.com/maps/d/u/0/embed?mid=177LE6fd5BeAqaQoNlnRpIY05wAqqz5_c'),
(3, 248, 9, 85, 87, 1, 0, 0, 'Trasan', 12, 'https://www.google.com/maps/d/u/0/embed?mid=1Z02pr5sOc4Isg3UjptEd9y11nUspPn6P'),
(4, 248, 9, 50, 56, 135, 0, 0, 'Trasan', 4, 'https://www.google.com/maps/d/u/0/embed?mid=1J7AMEk3Wyz636JBbLSmwcnWKNllHc7wm'),
(5, 248, 9, 50, 139, 194, 188, 196, 'Trasan', 12, 'https://www.google.com/maps/d/u/0/embed?mid=1Vk2Gb2QKb3tBlIlz0MJ04UlpCzs7ERPV'),
(6, 248, 124, 121, 129, 8, 0, 0, 'Trasan', 11, 'https://www.google.com/maps/d/u/0/embed?mid=1k55X9jYYuepuumXX9cP24vJciwY-bhBZ'),
(7, 248, 132, 10, 178, 0, 0, 0, 'Trasan', 24, 'https://www.google.com/maps/d/u/0/embed?mid=1GxhvU3f6V0iyFmSU3OVFO5Zh2iigZbeD'),
(8, 9, 125, 124, 207, 0, 0, 0, 'Trasan', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1gUsVlxbDOxuUJ76cIqc1cXHel5zBiqfe'),
(9, 229, 81, 37, 22, 10, 207, 0, 'Trasan', 14, 'https://www.google.com/maps/d/u/0/embed?mid=1d1yXHqNrwQZPZPeKOUE5WslLMjFP4-Sr'),
(10, 229, 81, 37, 11, 188, 0, 0, 'Trasan', 20, 'https://www.google.com/maps/d/u/0/embed?mid=1YuqTiOrVaQQVsi9vMAu3iuqBkGFXpSHE'),
(11, 81, 22, 9, 125, 55, 50, 37, 'Corta Distancia', 4, 'https://www.google.com/maps/d/u/0/embed?mid=1o-8Hik7jlHTvijaD-JWcGszLZw8sHnlx'),
(12, 100, 1, 81, 37, 9, 228, 0, 'Corta Distancia', 15, 'https://www.google.com/maps/d/u/0/embed?mid=1oW36endGs_hj-MsXRD8blWZoBAHafhqf'),
(13, 81, 37, 50, 25, 9, 0, 0, 'Corta Distancia', 15, 'https://www.google.com/maps/d/u/0/embed?mid=1mYyKV8NGchGqSvi9kK1mVujKayppyYqE'),
(14, 81, 1, 130, 126, 120, 137, 140, 'Corta Distancia', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1e3PEKWcmteXpxNQxk4hmkdjjv3wiriVt'),
(15, 81, 1, 130, 120, 140, 132, 127, 'Corta Distancia', 15, 'https://www.google.com/maps/d/u/0/embed?mid=1oMvS0fxDaNJXAb1yxh7mTtgLj3X2MHDY'),
(16, 187, 193, 186, 198, 188, 36, 59, 'Coomicro', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1f9V1UcgPh5rMRIkxRPMKIFW9kIWrJPuN'),
(17, 220, 205, 14, 229, 138, 120, 81, 'Coomicro', 20, 'https://www.google.com/maps/d/u/2/embed?mid=1xxmDFlVcPpmuRmpZ42l_5s-a5b-0nEvt'),
(18, 187, 193, 198, 200, 50, 1, 20, 'Coomicro', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1KJxs6FDsolaFpUu-__C0jCuoXbzMF6RX'),
(19, 187, 193, 117, 198, 188, 50, 56, 'Coomicro', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1FcLA0E5zhegyxCpWkIsTswgXB9Sx1Oj5'),
(20, 14, 229, 241, 100, 188, 28, 248, 'Coomicro', 20, 'https://www.google.com/maps/d/u/0/embed?mid=1nNo_y1PnwZ4bIAQCSK2Mesx-1G0BHcqj'),
(21, 14, 205, 208, 194, 172, 162, 117, 'Coomicro', 10, 'https://www.google.com/maps/d/u/0/embed?mid=14AM8yZtmy8eXlVRQZWjSJ6-Z_-s3CaOX'),
(22, 205, 206, 228, 138, 60, 80, 85, 'Coomicro', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1vKjgKq_P6v5kAjcFYGRVb6xGKLA53CC8'),
(23, 14, 220, 194, 140, 2, 56, 50, 'Coomicro', 15, 'https://www.google.com/maps/d/u/0/embed?mid=1RyBIjNU42F-QCzX_vQ3ERPvmpVqMwMjc'),
(24, 59, 240, 6, 9, 132, 172, 146, 'Guasimales', 8, 'https://www.google.com/maps/d/u/0/embed?mid=1qrVPeaGPzKGdPk3DXDKqC0tQGq3wzOh4'),
(25, 187, 193, 196, 228, 230, 239, 59, 'Guasimales', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1J8nXSW2AYzGomw7fscKYhsZ_rdQapX51'),
(26, 209, 206, 217, 6, 36, 59, 0, 'Guasimales', 10, 'https://www.google.com/maps/d/u/0/embed?mid=1ScUltz-r1p0BafIrnI5yMlgqe28WWmFo'),
(27, 193, 198, 188, 120, 140, 19, 59, 'Guasimales', 6, 'https://www.google.com/maps/d/u/0/embed?mid=1kN0k88HM_1JEIzmAPoKcq_TND3J8abXR'),
(28, 59, 22, 50, 56, 140, 162, 172, 'Guasimales', 5, 'https://www.google.com/maps/d/u/0/embed?mid=1tvo1wreirvvb-ppIUfFCFDiluv-EEvG2'),
(29, 102, 3, 103, 22, 248, 36, 59, 'Guasimales', 7, 'https://www.google.com/maps/d/u/0/embed?mid=1UNCgyskimoLVGWcvLfbIoN7wtuqhW6_I'),
(30, 59, 28, 22, 50, 23, 138, 121, 'Guasimales', 6, 'https://www.google.com/maps/d/u/0/embed?mid=1cFqrPNzRap2KO0BTtofWG77fYTdoLltQ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `barrios`
--
ALTER TABLE `barrios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `buseta`
--
ALTER TABLE `buseta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `barrios`
--
ALTER TABLE `barrios`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT de la tabla `buseta`
--
ALTER TABLE `buseta`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rutas`
--
ALTER TABLE `rutas`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
